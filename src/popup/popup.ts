// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import * as InvidiousManager from "../lib/API/invidious";
import {
    generateInvidiousInstancesDropdown,
    saveForm,
    selectFastestInstance,
    toggleCustomInstance,
    toggleEnable,
    updateDOM,
} from "../lib/form";
import * as InternationalizationManager from "../lib/i18n";
import * as SettingsManager from "../lib/settings";

/**
 * Initialize the popup
 * @returns {Promise<void>}
 */
const init = async () => {
    try {
        const settings = await SettingsManager.load();

        InternationalizationManager.replaceDOM();

        toggleEnable();
        updateDOM();
        generateInvidiousInstancesDropdown();

        if (await InvidiousManager.isCustomInstance(settings.invidious.appSettings.instance)) {
            toggleCustomInstance();
        }

        document.getElementById("invidition-form").addEventListener("change", saveForm);
        document.getElementById("isEnabled").addEventListener("change", toggleEnable);
        document.getElementById("invidious-appSettings-isEnabled").addEventListener("change", toggleEnable);
        document.getElementById("nitter-appSettings-isEnabled").addEventListener("change", toggleEnable);
        document.getElementById("invidious-button-custom-instance").addEventListener("click", toggleCustomInstance);
        document.getElementById("invidious-button-fastest-instance").addEventListener("click", selectFastestInstance);
        document.getElementById("invidition-openOptionsPage").addEventListener("click", () => {
            browser.runtime.openOptionsPage();
            return window.close();
        });
    } catch (e) {
        console.error(e.message);
    }
};

try {
    document.addEventListener("DOMContentLoaded", init);
} catch (e) {
    console.error(e);
}
