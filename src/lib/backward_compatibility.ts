// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import {config} from "../config/config";
import {error, info} from "./helpers";
import * as SettingManager from "./settings";

export const updateOldConfig = async (details) => {
    try {
        if (details.reason === "update") {
            if (details.previousVersion >= "0.11.0" && details.previousVersion < "0.13.0") {
                info("Updating configuration from previous version.");

                const settings = await SettingManager.load();

                config.invidious.instanceSettings.subtitles = settings.invidious.instanceSettings.captions;

                await SettingManager.save(config);
            } if (details.previousVersion < "0.11.0") {
                info("Updating configuration from previous version.");

                const settings = await browser.storage.sync.get();

                config.invidious.appSettings.instance = new URL(settings.instance).hostname;

                config.invidious.instanceSettings.autoplay = settings.autoplay === "1";
                config.invidious.instanceSettings.continue = settings.autoplay_next;
                config.invidious.instanceSettings.hl = settings.hl;
                config.invidious.instanceSettings.local = settings.local === "true";
                config.invidious.instanceSettings.subtitles = settings.subtitles;

                await SettingManager.clear(); // Remove old settings before saving new
                await SettingManager.save(config);
            }
        }
    } catch (e) {
        error(e.message);
    }
};

export const welcomePage = async (details) => {
    if (details.reason === "update") {
        if (details.previousVersion < "0.12.0") {
            const url = browser.runtime.getURL("/dist/welcome/welcome.html");

            browser.tabs.create({url, active: false});
        }
    }
};
