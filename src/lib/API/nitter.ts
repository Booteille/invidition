import {domainsToRedirect, domainToUrl} from "../helpers";
import * as SettingsManager from "../settings";

/**
 * Generate the Nitter Url
 *
 * @param {URL} oldUrl      The URL to apply settings
 * @returns {Promise<URL>}
 */
export const generateUrl = async (oldUrl: URL) => {
    try {
        const settings = await SettingsManager.load();
        let newUrl = domainToUrl(settings.nitter.appSettings.instance);

        if (oldUrl.hostname.indexOf("pbs.") !== -1) {
            newUrl.pathname = `/pic/${encodeURIComponent(oldUrl.href)}`;
        } else if (oldUrl.hostname.indexOf("video.") !== -1) {
            newUrl.pathname = `/gif/${encodeURIComponent(oldUrl.href)}`;
        } else {
            newUrl.pathname = oldUrl.pathname;
            newUrl.search = oldUrl.search;
        }

        return newUrl;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 *
 * @param r
 * @returns {Promise<boolean>}
 */
export const mustRedirect = async (r) => {
    try {
        const appSettings = (await SettingsManager.load()).nitter.appSettings;

        if (!appSettings.isEnabled) {
            return false;
        }

        const oldUrl = new URL(r.url);
        let newUrl = new URL(r.url);
        newUrl = await generateUrl(newUrl);
        const isNitterDomain = (await domainsToRedirect()).nitter.indexOf(oldUrl.hostname) !== -1;

        return isNitterDomain
            && (newUrl.hostname !== appSettings.instance || oldUrl.href !== newUrl.href);
    } catch (e) {
        console.error(e.message);
    }
};
