// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import * as InvidiousManager from "./API/invidious";

const copyLink = async (tab) => {
    try {
        console.log(tab.url);
        await navigator.clipboard.writeText(InvidiousManager.cleanedUrl(new URL(tab.url)).href);
    } catch (e) {
        console.error(e);
    }
};

const showCopyCleanedUrl = async (tabId: number) => {
    try {
        if (!(await browser.pageAction.isShown({tabId}))) {
            browser.pageAction.show(tabId);
            browser.pageAction.setTitle({
                                            tabId,
                                            title: browser.i18n.getMessage("copyCleanedLink"),
                                        });

            browser.pageAction.onClicked.addListener(copyLink)
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Show or hide the cleaned url page action depending on the current url
 * @returns {Promise<void>}
 */
export const toggleCleanable = async (tabId, changeInfo, tab) => {
    try {
        if (InvidiousManager.isCleanable(new URL(tab.url))) {
            showCopyCleanedUrl(tabId);
        }
    } catch (e) {
        console.error(e.message);
    }

    return {};
};
